from django.test import TestCase

from blog.forms import PostForm, DUPLICATE_POST_ERROR, EMPTY_POST_ERROR, ExistingBlogPostForm
from blog.models import Blog, Post


class PostFormTest(TestCase):

    def test_form_save_handles_saving_to_a_blog(self):
        blog = Blog.objects.create()
        form = PostForm(data={'text': 'do me'})
        new_post = form.save(for_blog=blog)
        self.assertEqual(new_post, Post.objects.first())
        self.assertEqual(new_post.text, 'do me')
        self.assertEqual(new_post.blog, blog)

    def test_form_post_input_has_placeholder_and_css_classes(self):
        form = PostForm()
        self.assertIn('placeholder="Enter some text ya dummy"', form.as_p())
        self.assertIn('class="form-control input-lg"', form.as_p())

    def test_form_validation_for_blank_posts(self):
        form = PostForm(data={'text': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['text'],
            ["You can't have an empty blog post"]
        )


class ExistingBlogPostFormTest(TestCase):

    def test_form_renders_post_text_input(self):
        blog_ = Blog.objects.create()
        form = ExistingBlogPostForm(for_blog=blog_)
        self.assertIn('placeholder="Enter some text ya dummy"', form.as_p())

    def test_form_validation_for_blank_posts(self):
        blog_ = Blog.objects.create()
        form = ExistingBlogPostForm(for_blog=blog_, data={'text': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors['text'], [EMPTY_POST_ERROR])

    def test_form_validation_for_duplicate_posts(self):
        blog_ = Blog.objects.create()
        Post.objects.create(blog=blog_, text='no twins!')
        form = ExistingBlogPostForm(for_blog=blog_, data={'text': 'no twins!'})
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors['text'], [DUPLICATE_POST_ERROR])

    def test_form_save(self):
        blog = Blog.objects.create()
        form = ExistingBlogPostForm(for_blog=blog, data={'text': 'hi'})
        new_item = form.save()
        self.assertEqual(new_item, Post.objects.first())
