from django.core.exceptions import ValidationError
from django.test import TestCase
from blog.models import Blog, Post


class PostModelTest(TestCase):

    def test_cannot_save_empty_list_items(self):
        list_ = Blog.objects.create()
        post = Post(blog=list_, text='')
        with self.assertRaises(ValidationError):
            post.save()
            post.full_clean()

    def test_duplicate_items_are_invalid(self):
        blog = Blog.objects.create()
        Post.objects.create(blog=blog, text='bla')
        with self.assertRaises(ValidationError):
            post = Post(blog=blog, text='bla')
            post.full_clean()

    def test_CAN_save_same_post_to_different_blog(self):
        blog_1 = Blog.objects.create()
        blog_2 = Blog.objects.create()

        Post.objects.create(blog=blog_1, text="bla")
        post = Post(blog=blog_2, text="bla")
        post.full_clean()

    def test_string_representation(self):
        post = Post(text='some text')
        self.assertEqual(str(post), 'some text')

    def test_default_text(self):
        post = Post()
        self.assertEqual(post.text, '')

    def test_post_is_related_to_list(self):
        blog = Blog.objects.create()
        post = Post()
        post.blog = blog
        post.save()
        self.assertIn(post, blog.post_set.all())


class BlogModelTest(TestCase):

    def test_get_absolute_url(self):
        blog = Blog.objects.create()
        self.assertEqual(blog.get_absolute_url(), f'/blog/{blog.id}/')
