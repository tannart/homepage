from .models import Post, Blog
from .forms import PostForm, ExistingBlogPostForm
from django.shortcuts import render, redirect
from django.views.generic import ListView, View


class NewBlogView(View):
    def get(self, request):
        return render(request, "blog.html", {"form": PostForm()})

    def post(self, request, *args, **kwargs):
        form = PostForm(data=request.POST)
        if form.is_valid():
            blog = Blog.objects.create()
            form.save(for_blog=blog)
            return redirect(blog)
        else:
            return render(request, 'blog.html', {'form': form})


class BlogPost(ListView):
    model = Post
    template_name = "post_list.html"
    context_object_name = "posts"

    def get(self, request, blog_id):
        blog = Blog.objects.get(id=blog_id)
        form = ExistingBlogPostForm(for_blog=blog)
        return render(request, self.template_name, {"blog": blog, "form": form})

    def post(self, request, blog_id, *args, **kwargs):
        blog = Blog.objects.get(id=blog_id)
        form = ExistingBlogPostForm(for_blog=blog, data=request.POST)
        if form.is_valid():
            form.save()
            return redirect(blog)
        return render(request, self.template_name, {'blog': blog, "form": form})
