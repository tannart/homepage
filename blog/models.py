from django.db import models
from django.urls import reverse


class Post(models.Model):
    text = models.TextField(default="")
    blog = models.ForeignKey("Blog", on_delete=models.CASCADE, default=None)

    class Meta:
        ordering = ('id', )
        unique_together = ('blog', 'text')

    def __str__(self):
        return self.text


class Blog(models.Model):

    def get_absolute_url(self):
        return reverse('blog:blog_item', args=[self.id])
