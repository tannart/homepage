from .base import FunctionalTest
from selenium.webdriver.common.keys import Keys


class ItemValidationTest(FunctionalTest):

    def get_error_element(self):
        return self.firefox.find_element_by_css_selector('.has-error')

    def test_cannot_add_empty_blog_post(self):
        # Edith goes to the home page and accidentally tries to submit
        # an empty post. She hits enter on the empty input box.
        self.firefox.get(self.live_server_url)
        self.firefox.find_element_by_id("blog").click()
        self.assertIn(self.firefox.title, 'Charlie Brodie: The Blog')
        self.get_item_input_box().send_keys(Keys.ENTER)

        # The home page refreshes and there is an error message saying
        # that posts cannot be blank
        self.wait_for(lambda: self.firefox.find_element_by_css_selector(
            '#id_text:invalid'
        ))

        # She tries again with some text for the item, which now works
        self.get_item_input_box().send_keys('Hello World')
        self.wait_for(lambda: self.firefox.find_element_by_css_selector(
            '#id_text:valid'
        ))
        self.get_item_input_box().send_keys(Keys.ENTER)
        self.wait_for_row_in_list('Hello World')

        # Perversely, she now decides to submit a second blank list item
        self.get_item_input_box().send_keys(Keys.ENTER)

        # She receives a similar warning on the list page
        self.wait_for(lambda: self.firefox.find_element_by_css_selector(
            '#id_text:invalid'
        ))

        # And she can correct it by filling some text in
        self.get_item_input_box().send_keys('Hello Universe')
        self.wait_for(lambda: self.firefox.find_element_by_css_selector(
            '#id_text:valid'
        ))
        self.get_item_input_box().send_keys(Keys.ENTER)
        self.wait_for_row_in_list('Hello World')
        self.wait_for_row_in_list('Hello Universe')

    def test_cannot_duplicate_posts(self):
        # Edith goes to the home page and starts a new list
        self.firefox.get(self.live_server_url)
        self.firefox.find_element_by_id("blog").click()
        self.assertIn(self.firefox.title, 'Charlie Brodie: The Blog')
        self.get_item_input_box().send_keys('Buy wellies')
        self.get_item_input_box().send_keys(Keys.ENTER)
        self.wait_for_row_in_list('Buy wellies')

        # She accidentally tries to enter a duplicate item
        self.get_item_input_box().send_keys('Buy wellies')
        self.get_item_input_box().send_keys(Keys.ENTER)

        # She sees a helpful error message
        self.wait_for(lambda: self.assertEqual(
            self.get_error_element().text,
            "You've already got this in your blog"
        ))

    def test_error_messages_are_cleared_on_input(self):
        # Edith starts a list and causes a validation error:
        self.firefox.get(self.live_server_url)
        self.firefox.find_element_by_id("blog").click()
        self.get_item_input_box().send_keys('Banter too thick')
        self.get_item_input_box().send_keys(Keys.ENTER)
        self.wait_for_row_in_list('Banter too thick')
        self.get_item_input_box().send_keys('Banter too thick')
        self.get_item_input_box().send_keys(Keys.ENTER)

        self.wait_for(lambda: self.assertTrue(
            self.get_error_element().is_displayed()
        ))

        # She starts typing in the input box to clear the error
        self.get_item_input_box().send_keys('a')

        # She is pleased to see that the error message disappears
        self.wait_for(lambda: self.assertFalse(
            self.get_error_element().is_displayed()
        ))
