from .base import FunctionalTest

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities


class NewVisitorTest(FunctionalTest):

    def test_homepage_contents(self):
        self.firefox.get(self.live_server_url)
        self.assertIn(self.firefox.title, 'Charlie Brodie: The Website')

        # Check the header has the correct text
        header = self.firefox.find_element_by_tag_name('h1').text
        self.assertEqual(header, "Charlie Brodie: The Website", msg="Header contained incorrect text")

        # Find and click the login button
        self.firefox.find_element_by_id('login').click()
        self.assertIn("The Login Page", self.firefox.title)

        # Return to the homepage
        self.firefox.back()

        # Find and click the register button
        self.firefox.find_element_by_id('register').click()
        self.assertIn("The Registration Page", self.firefox.title)

        # Check the contents of the register page
        input_elem = self.firefox.find_element_by_id("id_email")
        placeholder = input_elem.get_attribute("placeholder")
        self.assertEqual(placeholder, "Enter your email")

        # Return to the homepage
        self.firefox.back()

        # Check things are as they should be
        self.assertIn(self.firefox.title, 'Charlie Brodie: The Website')

    def test_blog_pages(self):
        self.firefox.get(self.live_server_url)
        self.assertIn(self.firefox.title, 'Charlie Brodie: The Website')

        self.firefox.find_element_by_id("blog").click()
        self.assertIn(self.firefox.title, 'Charlie Brodie: The Blog')

        inputbox = self.get_item_input_box()
        inputbox.send_keys("This is a new blog entry")
        inputbox.send_keys(Keys.ENTER)

        self.wait_for_row_in_list("This is a new blog entry")

        inputbox = self.get_item_input_box()
        inputbox.send_keys("This is another new blog entry")
        inputbox.send_keys(Keys.ENTER)

        self.wait_for_row_in_list("This is a new blog entry")
        self.wait_for_row_in_list("This is another new blog entry")

    def test_multiple_users_can_start_lists_at_different_urls(self):
        self.firefox.get(self.live_server_url)
        self.firefox.find_element_by_id('blog').click()
        inputbox = self.get_item_input_box()
        inputbox.send_keys('Blog post the first')
        inputbox.send_keys(Keys.ENTER)
        self.wait_for_row_in_list('Blog post the first')

        my_blog_url = self.firefox.current_url
        self.assertRegex(my_blog_url, '/blog/.+')

        self.firefox.quit()
        self.firefox = webdriver.Remote(
            command_executor='http://selenium:4444/wd/hub',
            desired_capabilities=DesiredCapabilities.FIREFOX
        )

        # Harley visits the homepage and does not see my blog posts
        self.firefox.get(self.live_server_url)
        self.firefox.find_element_by_id('blog').click()
        page_text = self.firefox.find_element_by_tag_name('body').text
        self.assertNotIn('Blog post the first', page_text)

        inputbox = self.get_item_input_box()
        inputbox.send_keys("I am Harley")
        inputbox.send_keys(Keys.ENTER)
        self.wait_for_row_in_list("I am Harley")

        # Harley gets his own unique url
        harley_list_url = self.firefox.current_url
        self.assertRegex(harley_list_url, '/blog/.+')
        self.assertNotEqual(harley_list_url, my_blog_url)

        page_text = self.firefox.find_element_by_tag_name('body').text
        self.assertNotIn('Blog post the first', page_text)
        self.assertIn('I am Harley', page_text)
