from django.shortcuts import render
from django.http import HttpResponse
from django.contrib.auth.views import LoginView
from django.contrib.auth.forms import UserCreationForm
from django.views.generic import FormView

# Create your views here.

def home_page(request):
    return render(request, "home.html")


class RegisterView(FormView):
    form_class = UserCreationForm
    template_name = "registration/signup.html"

    def post(self, request, *args, **kwargs):
        return render(request, "registration/incredulous_response.html", {
            "user_email": request.POST["email"],
            "password": request.POST["password1"]
        })
