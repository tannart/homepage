from django.test import TestCase


class SmokeTest(TestCase):

    def test_homepage_returns_correct_html(self):
        response = self.client.get("/")
        self.assertTemplateUsed(response, "home.html")

    def test_login_returns_correct_html(self):
        response = self.client.get("/login/")
        self.assertTemplateUsed(response, "registration/login.html")

    def test_register_returns_correct_html(self):
        response = self.client.get("/register/")
        self.assertTemplateUsed(response, "registration/signup.html")

    def test_can_save_post_request(self):
        response = self.client.post(
            "/register/",
            data={"email": "test_email@email.com", "password1": "test_password"},
        )
        self.assertIn("test_email@email.com", response.content.decode())
        self.assertIn(
            "Are you serious? You just put your email and password into some random website? And this website of all websites!!?!? You should try to exercise a little more caution in the future, I could be literally anyone and now I have your email, this is why you keep getting all that spam you know.",
            response.content.decode(),
        )
        self.assertTemplateUsed(response, "registration/incredulous_response.html")
